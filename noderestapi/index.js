// Import express
let express = require('./node_modules/express');
// Import Body parser
let bodyParser = require('./node_modules/body-parser');
// Import Mongoose
let mongoose = require('mongoose');
// Initialise the app
let app = express();

const {
    base64decode
  } = require('nodejs-base64');

// Import routes
let apiRoutes = require("./api-routes");
// Configure bodyparser to handle post requests
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
// Connect to Mongoose and set connection variable
mongoose.connect('mongodb://localhost/noderestapi', { useNewUrlParser: true});
var db = mongoose.connection;

// Added check for DB connection
if(!db)
    console.log("Error connecting db")
else
    console.log("Db connected successfully")

// Setup server port
var port = process.env.PORT || 8080;
  

// Send message for default URL
app.get('/', (req, res) => res.send('Hello World with Express'));


// custom basic authentication
app.use(function (req, res, next) {
    if (req.headers["authorization"]) {
      const authBase64 = req.headers['authorization'].split(' ');
      const userPass = base64decode(authBase64[1]);
      const user = userPass.split(':')[0];
      const password = userPass.split(':')[1];
  
      //
      if (user === 'admin' && password == '1234') {
        next();
        return;
      }
    }
    res.status(401);
    res.send({
      error: "Unauthorized "
    });
  });

// Use Api routes in the App
app.use('/api', apiRoutes);
// Launch app to listen to specified port

app.listen(port, function () {
    console.log("Running RestHub on port " + port);
});